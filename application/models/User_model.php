<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class User_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_users($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("users.*, roles.name AS role_name, cities.id AS city_id, cities.province_id AS province_id, cities.city_name AS city_name, provinces.province AS province_name, cities.postal_code AS postal_code");
			$this->db->where("users.delete", "active");
			
			if ($this->id) {
				$this->db->where("users.id", $this->id);
			}
			if ($filter['name'] != "") {
				$this->db->where("LOWER(users.nama) LIKE '%".strtolower($filter['name']."%'"));
			}

			if ($filter['email'] != "") {
				$this->db->where("LOWER(users.email) LIKE '%".strtolower($filter['email'])."%'");
			}

			if ($filter['date_join_start'] != "") {
				$this->db->where("users.created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			}

			$this->db->from("users");
			$this->db->join("roles", "roles.id = users.role_id");
			$this->db->join("cities", "cities.id = users.city_id");
			$this->db->join("provinces", "provinces.id = cities.province_id");
			$this->db->order_by("users.id DESC");
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_user($data){
			$this->db->where('id', $this->id);
			$this->db->update('users', $data);
		}

		function create_user($data){
			if($this->db->insert('users', $data)){
	            $insert_id = $this->db->insert_id();
	            return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('users', array('delete'=> 'deleted'));
		}
	}
?>