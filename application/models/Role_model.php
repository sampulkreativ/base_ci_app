<?php
	if (!defined('BASEPATH'))

    	exit('No direct script access allowed');
	class Role_model extends CI_Model {
		var $id;

		function __construct() {
			parent::__construct();
		}

		function get_roles($filter=null, $perpage = null, $from=null, $rows=null){
			$this->db->select("roles.*");
			$this->db->where("delete", "active");
			$this->db->from("roles");
			$this->db->order_by("id DESC");
			if ($this->id) {
				$this->db->where("id", $this->id);
			}
			if ($filter['name'] != "") {
				$this->db->where("LOWER(name) LIKE '%".$filter['name']."%'");
			}

			if ($filter['description'] != "") {
				$this->db->where("description", $filter['description']);
			}

			if ($filter['date_join_start'] != "") {
				$this->db->where("created_at BETWEEN '".$filter['date_join_start']."' AND '".$filter['date_join_end']."'");
			}
			
			if (isset($rows) == 1) {
				$db = $this->db->get();
			}else{
				$this->db->offset($from);
				$this->db->limit($perpage);
				$db = $this->db->get();
			}
			return $db;
		}

		function update_role($data){
			$this->db->where('id', $this->id);
			$this->db->update('roles', $data);
		}

		function create_role($data){
			if($this->db->insert('roles', $data)){
				$insert_id = $this->db->insert_id();
	            		return $insert_id;
			} else{
		        	return FALSE;
		      }
		}

		function destroy(){
			$this->db->where('id', $this->id);
			$this->db->update('roles', array('delete'=> 'deleted'));
		}

		function get_role_modules(){
			$this->db->select("get_role_modules.*");
			$this->db->from("get_role_modules");
			$this->db->where("role_id", $this->id);
			$this->db->order_by("role_module_id DESC");

			return $this->db->get();
		}

		function update_role_module($id, $data, $role_id){
			$this->db->where('id', $id);
			$this->db->where('role_id', $role_id);
			$this->db->update('role_modules', $data);
		}
	}
?>