<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 

class Simple_login {
	// SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
	}
	// Fungsi login
	public function login($username, $password) {
		$query = $this->CI->db->get_where('users',array('username'=>$username,'password' => $password));
		if($query->num_rows() == 1) {
			$row 	= $this->CI->db->query('SELECT * FROM users where username = "'.$username.'"');
			$admin 	= $row->row();
			$id 	= $admin->id;
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('user', $admin);
			$this->CI->session->set_userdata('id_login', uniqid(rand()));
			$this->CI->session->set_userdata('id', $id);
			redirect(base_url('dasbor'));
		}else{
			$this->CI->session->set_flashdata('sukses','Oops... Username/password salah');
			redirect(base_url('login'));
		}
		return false;
	}
	// Proteksi halaman
	public function cek_login() {
		if($this->CI->session->userdata('username') == '') {
			$this->CI->session->set_flashdata('sukses','Anda belum login');
			redirect(base_url('login'));
		}
	}

	public function current_user(){
		return $this->CI->session->userdata['user'];
	}

	public function check_role(){
		$action = $this->CI->uri->segment(2);
		$name = $this->CI->uri->segment(1);
		$alert = "Maaf, Anda tidak memiliki hak akses";
		$queries = $this->CI->db->query("SELECT * FROM get_roles_access WHERE user_id = ".$this->CI->session->userdata['user']->id." AND path = '".$name."'");

		foreach ($queries->result() as $role_module) {
			if($role_module->path == $name && $action == 'add' && $role_module->is_add == "0"){
				$this->CI->session->set_flashdata('alert',$alert);
				redirect(base_url('dasbor'));
			}
			if($role_module->path == $name && $action == 'edit' && $role_module->is_update == "0"){
				$this->CI->session->set_flashdata('alert',$alert);
				redirect(base_url('dasbor'));
			}
			if($role_module->path == $name && $action == 'destroy' && $role_module->is_delete == "0"){
				$this->CI->session->set_flashdata('alert',$alert);
				redirect(base_url('dasbor'));
			}
		}
	}

	// Fungsi logout
	public function logout() {
		$this->CI->session->unset_userdata('username');
		$this->CI->session->unset_userdata('user');
		$this->CI->session->unset_userdata('id_login');
		$this->CI->session->unset_userdata('id');
		$this->CI->session->set_flashdata('sukses','Anda berhasil logout');
		redirect(base_url('login'));
	}
}