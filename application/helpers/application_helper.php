<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function set_menus($current_user,$open_data_master, $open_data_system, $open_data_sales, $open_data_po, $open_data_administration, $open_data_bpb, $open_data_product, $open_data_global){
		$ci =& get_instance();
		$ci->load->helper('array');
		$ci->load->helper('url');
		$ci->load->database();
		$ci->db->select("modules.*");
		$ci->db->where("modules.delete", "active");
		$ci->db->where("modules.child", null);
		$ci->db->from("modules");
		$ci->db->order_by("menu_order ASC");
		$menus = $ci->db->get();
		$html = "";
		foreach ($menus->result() as $menu) {
			$html .= "<li class='has_sub'>";
			$html .= "<a href='' class='waves-effect ";
			$html .= isset($open_data_master) && $menu->active_class == "open_data_master" ? "active selected" : "";
			$html .= isset($open_data_system) && $menu->active_class == "open_data_system" ? "active selected" : "";
			$html .= isset($open_data_sales) && $menu->active_class == "open_data_sales" ? "active selected" : "";
			$html .= isset($open_data_po) && $menu->active_class == "open_data_purchase_order" ? "active selected" : "";
			$html .= isset($open_data_administration) && $menu->active_class == "open_data_administration" ? "active selected" : "";
			$html .= isset($open_data_bpb) && $menu->active_class == "open_data_bpb" ? "active selected" : "";
			$html .= isset($open_data_product) && $menu->active_class == "open_data_product" ? "active selected" : "";
			$html .= isset($open_data_global) && $menu->active_class == "open_data_global" ? "active selected" : "";			
			$html .= "'>";
			$html .= "<i class='".$menu->module_class."'></i>";
			$html .= "<span>".$menu->name."</span>";
			$html .= "</a>";
			$html .= "<ul class='list-unstyled'>";
			$mods = $ci->db->query('SELECT module_applications.*, modules.active_class AS active_class, modules.path_module AS path_module, modules.module_class AS module_class, modules.name AS module_name FROM module_applications INNER JOIN modules ON modules.id = module_applications.module_id WHERE module_id IN (SELECT id FROM modules where child = '.$menu->id.') AND modules.delete = "active" ORDER BY modules.id DESC');
			if ($mods != null){
				foreach ($mods->result() as $mod){
					$role_module = $ci->db->query('SELECT * FROM role_modules where role_id = '.$current_user->role_id.' AND module_application_id = '.$mod->id)->row();
					if ($role_module != null){
						if ($role_module->is_read){
							$html .= "<li class=";
							$html .= $ci->uri->segment(1) == $mod->active_class ? "active"  : $ci->input->get("type") == $mod->active_class ? "active" : "";
							$html .= ">";
							$html .= "<a href='".base_url().$mod->path_module."'><i class='".$mod->module_class."'></i> ".$mod->module_name."</a>";
							$html .= "</li>";
						}
					}
				}
				$html .= "</ul>";
				$html .= "</li>";
			}
		}
		return $html;
	}
?>