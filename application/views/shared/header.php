<div class="topbar">
	<!-- LOGO -->
	<div class="topbar-left">
		<div class="text-center">
			<a href="<?= base_url(); ?>" class="logo"><i class="icon-magnet icon-c-logo"></i><span>BASE<i class="md md-album"></i>APP</span></a>
		</div>
	</div>
	<div class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="pull-left">
				<button class="button-menu-mobile open-left">
					<i class="ion-navicon"></i>
				</button>
				<span class="clearfix"></span>
			</div>
			<div class="menu-extras">
				<ul class="nav navbar-nav navbar-right pull-right">
					<li class="dropdown navbar-c-items">
						<a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><img src="<?= base_url(); ?>assets/images/people.jpg" alt="user-img" class="img-circle"> </a>
						<ul class="dropdown-menu">
							<li><a href=""><i class="ti-settings text-custom m-r-10"></i> Pengaturan</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo base_url('login/logout'); ?>"><i class="ti-power-off text-danger m-r-10"></i> Keluar</a></li>
						</ul>
					</li>
				</ul>
				<div class="menu-item">
					<!-- Mobile menu toggle-->
					<a class="navbar-toggle">
						<div class="lines">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>