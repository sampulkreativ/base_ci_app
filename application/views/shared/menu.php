 <?php $this->load->helper('application'); ?>
 <div class="left side-menu">
	<div class="sidebar-inner slimscrollleft">
		<div id="sidebar-menu">
				<ul>
					<li class="text-muted menu-title">Navigasi</li>
					<li class="">
						<a href="<?=base_url();?>" class="waves-effect <?php echo isset($open_dasbor) ? $open_dasbor : '' ?>"><i class="fa fa-dashboard"></i> <span> Dasbor </span> </a>
					</li>
					<?php echo set_menus($this->simple_login->current_user(), isset($open_data_master) ? $open_data_master : null, isset($open_data_system) ? $open_data_system : null, isset($open_data_sales) ? $open_data_sales : null, isset($open_data_po) ? $open_data_po : null, isset($open_data_administration) ? $open_data_administration : null, isset($open_data_bpb) ? $open_data_bpb : null, isset($open_data_product) ? $open_data_product : null, isset($open_data_global) ? $open_data_global : null); ?>
				</ul>
				<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>