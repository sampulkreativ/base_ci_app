<form class="form-horizontal" action="<?=base_url();?>examples/index" method="get">
	<div class="col-sm-3" style="padding-left:0px">
		<input type="text" name="name" placeholder="Nama..." class="form-control input-sm" id="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : '' ?>">
	</div>
	<div class="col-sm-2">
		<input type="text" name="no_medtrack" placeholder="Nomor Medtrack..." class="form-control input-sm" id="no_medtrack" value="<?php echo isset($_GET['no_medtrack']) ? $_GET['no_medtrack'] : '' ?>">
	</div>
	<div class="col-sm-2">
		<input type="text" name="date_join_start" placeholder="Daftar Dari..." class="form-control input-sm datepicker" id="date_join_start" value="<?php echo isset($_GET['date_join_start']) ? $_GET['date_join_start'] : '' ?>">
	</div>
	<div class="col-lg-3">
		<div class="form-group input-group" style="margin:0">
			<input type="text" name="date_join_end" placeholder="Daftar Sampai..." class="form-control input-sm datepicker" id="date_join_end" value="<?php echo isset($_GET['date_join_end']) ? $_GET['date_join_end'] : '' ?>">
			<span class="input-group-btn">
				<a href="<?=base_url();?>examples/index" class="btn btn-white reset btn-sm"><i class="fa fa-refresh"></i></a>
				<?php echo form_submit(array('class' => 'btn btn-primary btn-sm', 'value' => 'Cari')); ?>
			</span>
		</div>
	</div>
</form>
<a href="<?=base_url();?>examples/add" class="btn btn-default waves-effect waves-light pull-right btn-sm"><i class="fa fa-plus"></i> Tambah
</a>
<div class="clearfix"></div>
<br>