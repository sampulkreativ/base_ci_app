<?php $this->load->view('shared/notice'); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="card-box">
			<?php $this->load->view('modules/shared/filter'); ?>
			<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
				<thead>
				    	<tr role="row">
					    	<th width="3%">No</th>
						<th width="15%">Nama Posisi</th>
						<th width="10%">Path Module</th>
						<th width="20%">Deskripsi</th>
						<th width="10%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(isset($modules) == 0 || $modules->num_rows() == null){
	                            echo"<td colspan='5'> <center><span> Data Tidak Tersedia </span></center> </td>";
	                      }else{
	                      	$no = 1 + $this->uri->segment(3); foreach ($modules->result() as $module) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $module->name ?></td>
								<td><?php echo $module->path_module ?></td>
								<td><?php echo $module->description ?></td>
								<td>
									<a href="<?= base_url(); ?>modules/show/<?php echo $module->id ?>" class="btn btn-default btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
									<a href="<?= base_url(); ?>modules/edit/<?php echo $module->id ?>" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>modules/destroy/<?php echo $module->id ?>" class="btn btn-danger btn-sm" onclick="return    confirm('Anda akan menghapus data, anda yakin ?');" title="Hapus"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>			
			<div class="col-sm-6" style="margin-top:20px">Menampilkan <b><?php echo $modules_all->num_rows() == 0 ? 0 : 1 ?></b> Data dari <b><?php echo $modules_all->num_rows() ?></b> Data</div>
			<div class="col-sm-6">
				<div class="pull-right">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>