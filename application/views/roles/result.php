<?php $this->load->view('shared/notice'); ?>
<?php $this->load->view('roles/shared/filter'); ?>
<div class="col-sm-12">
	<div class="card-box">
		<div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<div class="row"><div class="col-sm-12">
				<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
					<thead>
					    <tr role="row">
					    	<th width="3%">No</th>
							<th>Nama Posisi</th>
							<th width="20%">Deskripsi</th>
							<th width="15%">Tanggal Dibuat</th>
							<th width="15%">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<!--  $no = 1; foreach ($roles->result() as $role) { ?> -->
						<?php 
						if($results->result() == null){
                            echo"<td colspan='5'> <center><span> Data Tidak Tersedia </span></center> </td>";
                        }
                        else{
                        	$no = $this->uri->segment('3')+1; 
                                foreach ($results->result() as $role) { ?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $role->name ?></td>
								<td><?php echo $role->description ?></td>
								<td><?php echo $role->created_at ?></td>
								<td>
									 <?php
                                        echo" <a href='".base_url()."roles/edit/".$role->id ."' class='btn btn-white'><i class='ti-pencil'></i></a>";
                                        echo" <a href='".base_url()."roles/delete/".$role->id ."' onclick=\"return    confirm('Anda akan menghapus data, anda yakin ?');\" class='btn btn-white'><i class='ti-trash'></i></a>";
                                    ?>
								</td>
							</tr>
						<?php }	} ?>
					</tbody>
				</table>
                                    </div>
                                </div>