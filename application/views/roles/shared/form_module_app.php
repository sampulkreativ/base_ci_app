<table class="table table-bordered table-hover table-striped tablesorter">
	<thead>
		<tr>
			<th style="width:30%">Nama Modul</th>
			<th style="width:13%">Dapat Melihat</th>
			<th style="width:15%">Dapat Menambah</th>
			<th style="width:14%">Dapat Mengedit</th>
			<th style="width:15%">Dapat Menghapus</th>
		</tr>
	</thead>
	<tbody id="index_data">
		<?php foreach ($role_modules->result() as $role_module) { ?>
			<tr>
				<td><?php echo $role_module->module_name ?></td>
				<td>
					<center>
						<input type="checkbox" name="<?php echo $role_module->module_name ?>[is_read]" <?php echo $role_module->is_read ? "checked" : "" ?> id="is_read">
					</center>
				</td>
				<td>
					<center>
						<input type="checkbox" name="<?php echo $role_module->module_name ?>[is_add]" <?php echo $role_module->is_add ? "checked" : "" ?> id="is_add">
					</center>
				</td>
				<td>
					<center>
						<input type="checkbox" name="<?php echo $role_module->module_name ?>[is_update]" <?php echo $role_module->is_update ? "checked" : "" ?> id="is_update">
					</center>
				</td>
				<td>
					<center>
						<input type="checkbox" name="<?php echo $role_module->module_name ?>[is_delete]" <?php echo $role_module->is_delete ? "checked" : "" ?> id="is_delete">
					</center>
				</td>
			</tr>
			<input type="hidden" name="<?php echo $role_module->module_name ?>[module_application_id]" value="<?php echo $role_module->module_application_id ?>">
		<?php } ?>
	</tbody>
</table>