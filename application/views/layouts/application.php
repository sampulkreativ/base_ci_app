<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->simple_login->cek_login();
$this->simple_login->check_role();
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Base App</title>
	<!-- <base href="http://www.cyclistinsuranceaustralia.com.au/"> -->
	<link href="<?php echo base_url() ?>assets/stylesheet/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/core.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/components.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/icons.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/pages.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/menu.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/bootstrap-table.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/responsive.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/select2.css" rel="stylesheet">
	<script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
	<link href="<?php echo base_url() ?>assets/stylesheet/bootstrap-datepicker.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/stylesheet/datatables.css" rel="stylesheet">

</head>
<body class="fixed-left">
	<header id="topnav">
		<?php $this->load->view('shared/header'); ?>
		<?php $this->load->view('shared/menu'); ?>
	</header>
	<div class="content-page">
		<div class="content">
			<div class="container">
				<?php $this->load->view('shared/breadcum')?>
				<?php $this->load->view('shared/notice')?>
				<?php if($middle) echo $middle; ?>
				<?php $this->load->view('shared/modal')?>
			</div>
		</div>
		<!-- <?php $this->load->view('shared/footer'); ?> -->
	<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/detect.js"></script>
	<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap3-typeahead.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
	<script src="<?php echo base_url() ?>assets/js/waves.js"></script>
	<script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap-table.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.dashboard.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>
	<script src="<?php echo base_url() ?>assets/js/transaction.js"></script>
	<script src="<?php echo base_url() ?>assets/js/layanan_gigi.js"></script>
	<script src="<?php echo base_url() ?>assets/js/doctor.js"></script>
	<script src="<?php echo base_url() ?>assets/js/global.js"></script>
	<script src="<?php echo base_url() ?>assets/js/select2.js"></script>
	<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
</body>
<script type="text/javascript">
	$.fn.datepicker.defaults.format = "yyyy-mm-dd";
	$('.datepicker').datepicker();
	$(".select2").select2();
</script>
</html>