<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();	
		$this->load->model('Module_model');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->helper('date');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array(
			'child_null' => "",
			'name' => $this->input->get('name'),
			'path' => $this->input->get('path'),
			"description" => $this->input->get('description')
		);
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Data Module',
			'description' => 'Informasi Data Module',
			'modules_all' => $this->Module_model->get_modules($filter, null, null,'true')
		);
		$config = array(
			'base_url' => base_url().'modules/index/',
			'total_rows' => $data['modules_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['modules'] = $this->Module_model->get_modules($filter, $config['per_page'], $from, null);
		$this->middle = 'modules/index';
		$this->pagination->initialize($config);
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$filter = array(
			'child_null' => true
		);
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Data Module',
			'description' => 'Tambah Data Module',
			'childs' => $this->Module_model->get_childs()
		);
		$this->middle = 'modules/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Module';
		$data['description'] = 'Informasi Data Module';
		$data['open_data_system'] = 'active';
		$data['active_data_system_module'] = 'active';
		$data['childs'] = $this->Module_model->get_childs();
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('child', 'child', 'required');
		$this->form_validation->set_rules('path_module', 'path_module', 'required');
		$this->form_validation->set_rules('module_class', 'module_class', 'required');
		$this->form_validation->set_rules('active_class', 'active_class', 'required');
		$this->form_validation->set_rules('model_class', 'model_class', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'name' => $this->input->post('name', true),
				'child' => $this->input->post('child', true),
				'path_module' => $this->input->post('path_module', true),
				'module_class' => $this->input->post('module_class', true),
				'active_class' => $this->input->post('active_class', true),
				'model_class' => $this->input->post('model_class', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'modules/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'child' => $this->input->post('child', true),
				'path_module' => $this->input->post('path_module', true),
				'module_class' => $this->input->post('module_class', true),
				'active_class' => $this->input->post('active_class', true),
				'model_class' => $this->input->post('model_class', true),
				'description' => $this->input->post('description', true),
				'created_at' => mdate('%Y-%m-%d')
			);
			$id = $this->Module_model->create_module($data);
			$this->Module_model->new_child();
			redirect('modules/show/'.$id);
		}
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->Module_model->id = $id;
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Detail Module',
			'description' => 'Informasi Detail Module',
			'module' => $this->Module_model->get_modules()->row(),
			'childs' =>  $this->Module_model->get_childs()
		);

		$this->middle = 'modules/show';
		$this->data = $data;
		$this->layout();
	}

	function edit($id){
		$filter = array(
			'child_null' => true
		);

		$this->Module_model->id = $this->uri->segment(3);
		$data = array(
			'open_data_system' => 'active',
			'page_title' => 'Edit Module',
			'description' => 'Form Edit Module',
			'module' => $this->Module_model->get_modules()->row(),
			'childs' => $this->Module_model->get_childs()
		);
		$module = $data['module'];
		$data['value'] = array(
			'id' => $module->id,
			'name' => $module->name,
			'child' => $module->child,
			'path_module' => $module->path_module,
			'module_class' => $module->module_class,
			'active_class' => $module->active_class,
			'model_class' => $module->model_class,
			'description' => $module->description
		);
		$this->middle = 'modules/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$id = $this->input->post('id', true);
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('child', 'child', 'required');
		$this->form_validation->set_rules('path_module', 'path_module', 'required');
		$this->form_validation->set_rules('module_class', 'module_class', 'required');
		$this->form_validation->set_rules('active_class', 'active_class', 'required');
		$this->form_validation->set_rules('model_class', 'model_class', 'required');
		$data['childs'] = $this->Module_model->get_childs();

		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' => $id,
				'name' => $this->input->post('name', true),
				'child' => $this->input->post('child', true),
				'path_module' => $this->input->post('path_module', true),
				'module_class' => $this->input->post('module_class', true),
				'active_class' => $this->input->post('active_class', true),
				'model_class' => $this->input->post('model_class', true),
				'description' => $this->input->post('description', true)
			);
			$this->middle = 'modules/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'child' => $this->input->post('child', true),
				'path_module' => $this->input->post('path_module', true),
				'module_class' => $this->input->post('module_class', true),
				'active_class' => $this->input->post('active_class', true),
				'model_class' => $this->input->post('model_class', true),
				'description' => $this->input->post('description', true)
			);
			$this->Module_model->id = $id;
			$this->Module_model->update_module($data);
			redirect('modules/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->Module_model->id = $this->uri->segment(3);
		$this->Module_model->destroy();
		redirect('modules/index');
	}
}