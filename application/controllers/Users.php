<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public $template = array();
	public $data = array();

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Role_model');
		$this->load->model('Provcity_model');		
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->library('pagination');
		$this->load->library('form_validation');
    	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	function index(){
		$filter = array(
			'name' => $this->input->get("name"),
			'email' => $this->input->get('email'),
			'date_join_start' => $this->input->get('date_join_start'),
			'date_join_end' => $this->input->get('date_join_end')
		);

		$data = array(
			'open_data_master' => 'active',
			'active_data_user' => 'active',
			'page_title' => 'Data Pengguna',
			'description' => 'Informasi Data Pengguna',
			'users_all' => $this->User_model->get_users($filter, null, null,'true')
		);

		$config = array(
			'base_url' => base_url().'users/index/',
			'total_rows' => $data['users_all']->num_rows(),
			'per_page' =>  20,
			'full_tag_open' => "<ul class='pagination'>",
			'full_tag_close' => "</ul>",
			'num_tag_open' => "<li class='paginate_button'>",
			'num_tag_close' =>  "</li>",
			'cur_tag_open' => "<li class='paginate_button active' ><a class='current'>", 
			'cur_tag_close' =>  "</li>",
			'next_tag_open' =>  "<li class='paginate_button next'>",
			'next_tagl_close' =>  "</li>",
			'prev_tag_open' =>  "<li class='paginate_button previous disabled'>",
			'prev_tagl_close' =>  "</li>",
			'first_tag_open' =>  "<li class='paginate_button'>",
			'first_tagl_close' =>  "</li>",
			'last_tag_open' =>  "<li class='paginate_button'>",
			'last_tagl_close' =>  "</li>",
			'first_link' =>  "<< Pertama",
			'last_link' =>  "Terakhir >>",
			'next_link' =>  "Next >",
			'prev_link' =>  "< Prev"
		);

		$from = $this->uri->segment(3);
		$data['users'] = $this->User_model->get_users($filter, $config['per_page'], $from, null);
		$this->pagination->initialize($config);
		$this->middle = 'users/index';
		$this->data = $data;
		$this->layout();
	}

	function show(){
		$id = $this->uri->segment(3);
		$this->User_model->id = $id;
		$data = array(
			'open_data_master' => 'active',
			'active_data_user' => 'active',
			'page_title' => 'Detail Pengguna',
			'description' => 'Informasi Detail Pengguna',
			'user' => $this->User_model->get_users()->row()
		);

		$this->middle = 'users/show';
		$this->data = $data;
		$this->layout();
	}

	function add(){
		$data = array(
			'open_data_master' => 'active',
			'active_data_user' => 'active',
			'page_title' => 'Data Pengguna',
			'description' => 'Tambah Data Pengguna',
			'roles' => $this->Role_model->get_roles(null,null,null,null),
			'provinces' => $this->Provcity_model->get_provinces(),
			'cities' => $this->Provcity_model->get_cities()
		);
		$this->middle = 'users/new';
		$this->data = $data;
		$this->layout();
	}

	function create(){
		$data = array();
		$data['page_title'] = 'Data Pengguna';
		$data['description'] = 'Informasi Data Pengguna';
		$data['open_data_master'] = 'active';
		$data['active_data_user'] = 'active';
		$data['roles'] = $this->Role_model->get_roles(null,null,null,null);	
		$data['provinces'] = $this->Provcity_model->get_provinces();
		$data['cities'] = $this->Provcity_model->get_cities();
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('role_id', 'role_id', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[users.email]');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('city_id', 'city_id', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'name' => $this->input->post('name', true),
				'username' => $this->input->post('username', true),
				'role_id' => $this->input->post('role_id', true),
				'email' => $this->input->post('email', true),
				'phone' => $this->input->post('phone', true),
				'city_id' => $this->input->post('city_id', true),
				'address' => $this->input->post('address', true),
				'password' => md5($this->input->post('password', true))
			);
			$this->middle = 'users/new';
			$this->data = $data;
			$this->layout();
		}else{
			$data = array(
				'name' => $this->input->post('name', true),
				'username' => $this->input->post('username', true),
				'role_id' => $this->input->post('role_id', true),
				'email' => $this->input->post('email', true),
				'phone' => $this->input->post('phone', true),
				'city_id' => $this->input->post('city_id', true),
				'address' => $this->input->post('address', true),
				'password' => md5($this->input->post('password', true)),
				'created_at' => mdate('%Y-%m-%d')
			);
			$id = $this->User_model->create_user($data);
			redirect('users/show/'.$id);
		}		
	}

	function edit($id){
		$data = array(
			'open_data_master' => 'active',
			'active_data_user' => 'active',
			'page_title' => 'Edit Pengguna',
			'description' => 'Form Edit Pengguna',
			'roles' => $this->Role_model->get_roles(null,null,null,null),
			'provinces' => $this->Provcity_model->get_provinces(),
			'cities' => $this->Provcity_model->get_cities()
		);

		$this->User_model->id = $this->uri->segment(3);
		$data['user'] = $this->User_model->get_users()->row();
		$user = $this->User_model->get_users()->row();
		$data['value'] = array(
			'id' => $user->id,
			'username' => $user->username,
			'email' => $user->email,
			'name' => $user->name,
			'phone' => $user->phone,
			'city_id' => $user->city_id,
			'province_id' => $user->province_id,
			'address' => $user->address,
			'role_id' => $user->role_id
		);
		$this->middle = 'users/edit';
		$this->data = $data;
		$this->layout();
	}

	function update(){
		$data = array();
		$data['page_title'] = 'Data Pengguna';
		$data['description'] = 'Informasi Data Pengguna';
		$data['open_data_master'] = 'active';
		$data['active_data_user'] = 'active';
		$data['roles'] = $this->Role_model->get_roles(null,null,null,null);
		$data['provinces'] = $this->Provcity_model->get_provinces();
		$data['cities'] = $this->Provcity_model->get_cities();
		$id = $this->input->post('id', true);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('city_id', 'city_id', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error_form_validation = preg_split('/\r\n|\n|\r/', $this->form_validation->error_string());
			if ($this->form_validation->error_string()){
				unset($error_form_validation[count($error_form_validation) - 1]);
				$error_form = array_merge($error_form_validation);
				$data['errors'] = array($this->form_validation->error_string());
			} else {
				$data['errors'] = "Data must be filled";
			}
			$data['value'] = array(
				'id' =>  $this->input->post('id', true),
				'name' => $this->input->post('name', true),
				'username' => $this->input->post('username', true),
				'role_id' => $this->input->post('role_id', true),
				'email' => $this->input->post('email', true),
				'phone' => $this->input->post('phone', true),
				'city_id' => $this->input->post('city_id', true),
				'address' => $this->input->post('address', true),
				'password' => md5($this->input->post('password', true))
			);
			$this->middle = 'users/new';
			$this->data = $data;
			$this->layout();
		}else{
			$id = $this->input->post('id', true);
			$data = array(
				'name' => $this->input->post('name', true),
				'username' => $this->input->post('username', true),
				'role_id' => $this->input->post('role_id', true),
				'email' => $this->input->post('email', true),
				'phone' => $this->input->post('phone', true),
				'city_id' => $this->input->post('city_id', true),
				'address' => $this->input->post('address', true),
				'password' => md5($this->input->post('password', true))
			);
			$this->User_model->id = $id;
			$this->User_model->update_user($data);
			redirect('users/show/'.$id);
		}
	}

	function destroy($id){
		$this->simple_login->check_role();
		$this->User_model->id = $this->uri->segment(3);
		$this->User_model->destroy();
		redirect('users/index');
	}

	function select_ajax(){
		$filter = array(
			'province_id' => $this->input->get('province_id')
		);
		$uri3 = $this->uri->segment(3);
		$cities = $this->Provcity_model->get_cities($filter)->result();
		$html = "";
		foreach ($cities as $city) {
			$html .= "<option value='";
			$html .= $city->id;
			$html .= "'";
			$html .= isset($user) ? ($user->city_id == $city->id ? "selected" : "") : "";
			$html .= ">";
			$html .= $city->city_name;
			$html .= " | ";
			$html .= $city->postal_code;
			$html .= "</option>";	
		}
		$data = array("cities" => $html);
		echo json_encode($data);
	}
}