<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {
	public $template = array();
	public $data = array();

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index() {
		$data = array(
			'open_dasbor' => 'active',
			'page_title' => 'Dasbor',
			'description' => 'Selamat Datang di Dasbor'
		);
		$this->middle = 'dasbor/index';
		$this->data = $data;
		$this->layout();
	}
}