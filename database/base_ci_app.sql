-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 19, 2018 at 09:13 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `base_ci_app`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `get_roles_access`
-- (See below for the actual view)
--
CREATE TABLE `get_roles_access` (
`id` int(5)
,`role_id` int(5)
,`module_application_id` int(5)
,`is_read` tinyint(1)
,`is_add` tinyint(1)
,`is_update` tinyint(1)
,`is_delete` tinyint(1)
,`user_id` int(10) unsigned
,`path` varchar(25)
,`module_id` int(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `get_role_modules`
-- (See below for the actual view)
--
CREATE TABLE `get_role_modules` (
`role_id` int(11)
,`role_module_id` int(5)
,`module_application_id` int(5)
,`module_id` int(5)
,`module_name` varchar(20)
,`is_read` tinyint(1)
,`is_add` tinyint(1)
,`is_update` tinyint(1)
,`is_delete` tinyint(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `path_module` varchar(25) NOT NULL,
  `module_class` varchar(25) NOT NULL,
  `created_at` date NOT NULL,
  `active_class` varchar(20) NOT NULL,
  `child` int(11) DEFAULT NULL,
  `model_class` varchar(25) NOT NULL,
  `menu_order` int(3) NOT NULL,
  `delete` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `path_module`, `module_class`, `created_at`, `active_class`, `child`, `model_class`, `menu_order`, `delete`) VALUES
(9, 'Master Data', 'lorem ipsum', '#', 'fa fa-table', '2018-04-18', 'open_data_master', NULL, '', 1, 'active'),
(10, 'Pengguna', '', 'users', 'fa fa-circle-o', '2018-04-18', 'users', 9, 'User_model', 0, 'active'),
(11, 'Hak Akses', 'lorem ipsum dolor sit amet', 'roles', 'fa fa-circle-o', '2018-04-18', 'roles', 9, 'Role_model', 0, 'active'),
(12, 'Module', 'lorem ipsum dolor sit amet', 'modules', 'fa fa-circle-o', '2018-04-18', 'modules', 9, 'Module_model', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `module_applications`
--

CREATE TABLE `module_applications` (
  `id` int(5) NOT NULL,
  `module_id` int(5) NOT NULL,
  `action_read` varchar(30) NOT NULL,
  `action_create` varchar(30) NOT NULL,
  `action_update` varchar(30) NOT NULL,
  `action_destroy` varchar(30) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_applications`
--

INSERT INTO `module_applications` (`id`, `module_id`, `action_read`, `action_create`, `action_update`, `action_destroy`, `created_at`) VALUES
(3, 9, 'can :read, ', 'can :create, ', 'can :update, ', 'can :destroy, ', '0000-00-00'),
(4, 10, 'can :read, User_model', 'can :create, User_model', 'can :update, User_model', 'can :destroy, User_model', '0000-00-00'),
(5, 11, 'can :read, Role_model', 'can :create, Role_model', 'can :update, Role_model', 'can :destroy, Role_model', '0000-00-00'),
(6, 12, 'can :read, Module_model', 'can :create, Module_model', 'can :update, Module_model', 'can :destroy, Module_model', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `delete`, `created_at`) VALUES
(1, 'Superadmin', 'superadmin', 'active', '2018-04-16'),
(2, 'Admin', 'lorem', 'deleted', '2018-04-16'),
(3, 'Import Master', 'loremasita', 'deleted', '2018-04-16');

-- --------------------------------------------------------

--
-- Table structure for table `role_modules`
--

CREATE TABLE `role_modules` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `module_application_id` int(5) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '1',
  `is_add` tinyint(1) NOT NULL DEFAULT '1',
  `is_update` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_modules`
--

INSERT INTO `role_modules` (`id`, `role_id`, `module_application_id`, `is_read`, `is_add`, `is_update`, `is_delete`, `created_at`) VALUES
(2, 1, 3, 1, 1, 1, 1, '0000-00-00'),
(3, 1, 4, 1, 0, 1, 0, '0000-00-00'),
(4, 1, 5, 1, 1, 1, 0, '0000-00-00'),
(5, 1, 6, 1, 1, 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role_id` int(5) NOT NULL,
  `created_at` date NOT NULL,
  `delete` varchar(10) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `role_id`, `created_at`, `delete`) VALUES
(1, 'Loreal', 'andoyoandoyo@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 1, '2018-04-16', 'deleted'),
(2, 'Lorem ipsum dolor Hasan', 'dolor@lorem.com', 'lorem_ipsum', 'd41d8cd98f00b204e9800998ecf8427e', 1, '2018-04-03', 'deleted'),
(3, 'Hasan Al Farisi', 'learn@kiranatama.com', 'lorem', 'd2e16e6ef52a45b7468f1da56bba1953', 1, '2018-04-15', 'deleted'),
(4, 'Hasan Al Farisi', 'learn@gmail.com', 'hasan', '25d55ad283aa400af464c76d713c07ad', 1, '2018-04-16', 'active'),
(5, 'role_percobaan', 'role@role.com', 'rolade', 'a516ff74e222c3bbd3ab924e96fefe0c', 1, '2018-04-19', 'deleted');

-- --------------------------------------------------------

--
-- Structure for view `get_roles_access`
--
DROP TABLE IF EXISTS `get_roles_access`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `get_roles_access`  AS  select `role_modules`.`id` AS `id`,`role_modules`.`role_id` AS `role_id`,`role_modules`.`module_application_id` AS `module_application_id`,`role_modules`.`is_read` AS `is_read`,`role_modules`.`is_add` AS `is_add`,`role_modules`.`is_update` AS `is_update`,`role_modules`.`is_delete` AS `is_delete`,`users`.`id` AS `user_id`,`modules`.`path_module` AS `path`,`modules`.`id` AS `module_id` from ((((`role_modules` join `roles` on((`roles`.`id` = `role_modules`.`role_id`))) join `users` on((`users`.`role_id` = `roles`.`id`))) join `module_applications` on((`module_applications`.`id` = `role_modules`.`module_application_id`))) join `modules` on((`modules`.`id` = `module_applications`.`module_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `get_role_modules`
--
DROP TABLE IF EXISTS `get_role_modules`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `get_role_modules`  AS  select `roles`.`id` AS `role_id`,`role_modules`.`id` AS `role_module_id`,`module_applications`.`id` AS `module_application_id`,`modules`.`id` AS `module_id`,`modules`.`name` AS `module_name`,`role_modules`.`is_read` AS `is_read`,`role_modules`.`is_add` AS `is_add`,`role_modules`.`is_update` AS `is_update`,`role_modules`.`is_delete` AS `is_delete` from (((`role_modules` join `module_applications` on((`module_applications`.`id` = `role_modules`.`module_application_id`))) join `modules` on((`modules`.`id` = `module_applications`.`module_id`))) join `roles` on((`roles`.`id` = `role_modules`.`role_id`))) where (`modules`.`child` is not null) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_applications`
--
ALTER TABLE `module_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_modules`
--
ALTER TABLE `role_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `module_applications`
--
ALTER TABLE `module_applications`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_modules`
--
ALTER TABLE `role_modules`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
