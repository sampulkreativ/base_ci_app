var selectGigi = {
	gigiautocomplete: function(){
		$('#select_gigi').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},{
			source: function (query, processSync, processAsync) {
			return $.ajax({
				url: "/mytrex/transaction/get_thoots", 
				type: 'GET',
				data: {query: query},
				dataType: 'json',
					success: function (data) {
						if (data.options != "") {
							return processAsync(data.options);
						}
					}
				});
			}
		});
	}
}

selectGigi.gigiautocomplete();
$('body').on("change", "#select_gigi", function(){
	$.ajax({
		url: "/mytrex/transaction/get_thoot", 
		type: 'GET',
		data: {query: $(this).parent().parent().parent().find("#select_gigi").val()},
		dataType: 'json',
		success: function (data) {
			if (data.harga != "") {
				$("#price_gigi").html(data.harga);
				$("#medical_tooth_id").val(data.id);
				$("#subtotal_gigi").html(data.harga);
			}
		}
	});
});
var index = 0;
var gigi = {
	addgigi: function(_this){
		var parent_element = _this.parent().parent();
		var clone_element  = parent_element.clone();
		parent_element.find("#gigi").parent().append("<span>"+$("#select_gigi").val()+"</span>");
		parent_element.append("<input type='hidden' name='counting_gigi' value='"+index+"' />")
		parent_element.append("<input type='hidden' name='medical_tooth_id_"+index+"' value='"+$("#medical_tooth_id").val()+"' />");
		parent_element.append("<input type='hidden' name='tab' value='gigi'/>");
		parent_element.append("<input type='hidden' name='gigi_total_"+index+"' value='"+parseInt($("#price_gigi").html())+"' />")
		parent_element.find("#subtotal_gigi").parent().html("<span> Rp."+parseInt($("#price_gigi").html())+",-</span>");
		parent_element.find("#price_gigi").removeAttr('id');
		//parent_element.find("#subtotal").removeAttr('id').addClass('subtotal');
		parent_element.find("#gigi, #select_gigi").remove();
		parent_element.hide().fadeIn("slow");
		//parent_element.find("#s2id_drug").get(0).remove();

		_this.find(".fa-plus").addClass("fa-trash");
		_this.removeClass("add-line-gigi").addClass("remove-line-gigi");
		_this.attr("title","Hapus Baris");

		clone_element.find("#gigi, #select_gigi").val("");
		clone_element.find("#price_gigi, #subtotal, #subtotal_gigi").html("");
		clone_element.find("#price_gigi, #gigi").next().val("");
		$("#details-gigi").append(clone_element);
		index += 1;
	}
}

$('body').on("click", ".add-line-gigi", function(){
	var _this = $(this);
 	gigi.addgigi(_this);
	selectGigi.gigiautocomplete();	
});

$('body').on("click", ".remove-line-gigi", function(){
	var _this      = $(this);
	var _this_line = _this.parent().parent();
	var confirmBox = confirm("hapus baris layanan gigi?");
	if (confirmBox == true){
		_this_line.fadeOut('slow', function(){
			_this_line.remove();
		});
	}
});

$("body").on("click",".remove-permanent-gigi", function(){
	var _this      = $(this);
	var _this_line = _this.parent().parent();
	var confirmBox = confirm("hapus baris Layanan Gigi?");
	if (confirmBox == true){
		_this_line.fadeOut('slow', function(){
			_this_line.remove();
		});
		$.ajax({
			url: "/mytrex/transaction/remove_tooths", 
			type: 'GET',
			data: {id: _this.attr("dataId"), medical_tooth_id: _this.attr("medicalToothId"), consul_registration_id: _this.attr("dataConsulRegistrationId")},
			dataType: 'json'
		});
		location.reload(); 
	}
})