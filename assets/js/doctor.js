var index = 0;
var schedule = {
	addschedule: function(_this){
		var parent_element = _this.parent().parent();
		var clone_element  = parent_element.clone();
		parent_element.find("#schedule").parent().append("<span>"+document.getElementById('day')[document.getElementById('day').selectedIndex].innerHTML+"</span>");
		parent_element.find("#append_start_time").parent().append("<span>"+$("#start_time").val()+"</span>");
		parent_element.find("#append_end_time").parent().append("<span>"+$("#end_time").val()+"</span>");
		parent_element.append("<input type='hidden' name='counting_schedule' value='"+index+"' />")
		parent_element.append("<input type='hidden' name='day_"+index+"' value='"+$("#day").val()+"' />")
		parent_element.append("<input type='hidden' name='start_time_"+index+"' value='"+$("#start_time").val()+"' />")
		parent_element.append("<input type='hidden' name='end_time_"+index+"' value='"+$("#end_time").val()+"' />")
		parent_element.find("#day, #start_time, #end_time").remove();
		parent_element.hide().fadeIn("slow");
		//parent_element.find("#s2id_drug").get(0).remove();

		_this.find(".fa-plus").addClass("fa-trash");
		_this.removeClass("add-line-schedule").addClass("remove-line-schedule");
		_this.attr("title","Hapus Baris");

		clone_element.find("#start_time, #end_time").val("");
		clone_element.find("#start_time, #end_time").next().val("");
		$("#details-schedule").append(clone_element);
		index += 1;
	}
}

$('body').on("click", ".add-line-schedule", function(){
	var _this = $(this);
 	schedule.addschedule(_this);
});

$('body').on("click", ".remove-line-schedule", function(){
	var _this      = $(this);
	var _this_line = _this.parent().parent();
	var confirmBox = confirm("hapus baris jadwal?");
	if (confirmBox == true){
		_this_line.fadeOut('slow', function(){
			_this_line.remove();
		});
	}
});