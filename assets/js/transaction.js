$('#supplier').typeahead({
	hint: true,
	highlight: true,
	minLength: 1
},{
	source: function (query, processSync, processAsync) {
	return $.ajax({
		url: "/mytrex/transaction/get_suppliers", 
		type: 'GET',
		data: {query: query},
		dataType: 'json',
			success: function (data) {
				if (data.options != "") {
					return processAsync(data.options);
				}
			}
		});
	}
});

$('body').on("click", ".tt-suggestion", function(){
	$.ajax({
		url: "/mytrex/transaction/get_suppliers", 
		type: 'GET',
		data: {query: $(this).parent().parent().parent().find("#supplier").val()},
		dataType: 'json',
		success: function (data) {
			if (data.options != "") {
				$("#supplier_address").val(data.address);
				$("#supplier_id").val(data.id);
			}
		}
	});
});